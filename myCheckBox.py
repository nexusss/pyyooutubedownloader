import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import (QWidget,QVBoxLayout,QHBoxLayout,QCheckBox,QLabel,QScrollArea)
from PyQt5.QtCore import QObject, pyqtSignal

class MyCheckBox(QCheckBox):
    checkId = 0
    checkedSignal = pyqtSignal(int,int)

    def __init__(self,title, id):
        super().__init__(title)
        
        self.checkId = id
        self.stateChanged.connect(lambda: 
                                        self.checkedSignal.emit(self.isChecked(),self.checkId)
                                        )

def proc(state, id):
    print("id")
    print(state)
    print(id)

def proc2(state):
    print("state")
    print(state == False)
    print(state == True)

if __name__ == '__main__':
    print('start')
    app = QApplication(sys.argv)
    ex = MyCheckBox("asdasd",12)
    ex.show()
    ex.checkedSignal.connect(proc)
    ex.stateChanged.connect(proc2)
    # ex.downloadYoutubeUrl()

    sys.exit(app.exec_())