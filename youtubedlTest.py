import sys
import youtube_dl
from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtWidgets import QMainWindow, QApplication

class myYouTubeDownloader(QObject):
    downloadProgress = pyqtSignal(int)
    totalBytesChanged = pyqtSignal(int)
    finishedDownload = pyqtSignal()
    sendTitle = pyqtSignal('QString')
    sendDescription = pyqtSignal('QString')
    sendFormat = pyqtSignal([list])

    ydl_opts = {'id':137}
    ydl = youtube_dl.YoutubeDL(ydl_opts)
    urls = [""]
    # with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    
    #print("start download")
    #print(ydl.list_formats(['https://www.youtube.com/watch?v=dP15zlyra3c']))
    #ydl.download(['https://www.youtube.com/watch?v=UTQbrjR03W8'])
    
    def progress_hook(self,status):
        if(status['status'] == 'downloading'):
            self.downloadProgress.emit(status['downloaded_bytes'])
            self.totalBytesChanged.emit(status['total_bytes'])
            return
        if(status['status'] == 'finished'):
            self.finishedDownload.emit()
            return

    def __init__(self):
        super().__init__()
        self.initUI()
        
    def initUI(self):
        print('progress add')
        self.ydl.add_progress_hook(self.progress_hook)
        print(self.ydl.params)

    def downloadYoutubeUrl(self):
        self.ydl.download(self.urls) 

    def setUrl(self,url):
        self.urls = [url]
        info_dict = []
        print("info_dict")
        result = self.ydl.extract_info(url,False)
        print("set url")
        self.sendTitle.emit(result['title'])
        self.sendDescription.emit(result['description'])
        formats = result['formats']
        self.sendFormat.emit(formats)


if __name__ == '__main__':
    print('start')
    app = QApplication(sys.argv)
    ex = myYouTubeDownloader()
    ex.setUrl('https://www.youtube.com/watch?v=UTQbrjR03W8')

    ex.downloadYoutubeUrl()

    sys.exit(app.exec_())
    