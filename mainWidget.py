import sys
from PyQt5.QtWidgets import (QApplication, QWidget,QLineEdit,QLabel,QPushButton,
QProgressBar,QVBoxLayout,QHBoxLayout,QCheckBox)
from PyQt5.QtCore import QThread

from youtubedlTest import myYouTubeDownloader
from settingsWidget import SettingsWidget

class MainWidget(QWidget):
    
    def __init__(self):
        super().__init__()
        self.initUI()
    
    def procFormat(self,formats):
        print("proc formats")
        
        # for i in reversed(range(self.settingsBox.count())): 
            # self.settingsBox.itemAt(i).widget().deleteLater()

        print(formats)
        for ff in formats:
            if 'audio' in ff['format_note']:
                print('audio')
            else:
                # checkBox = QCheckBox(ff['format_note'] + str(ff['width']) + "x" + str(ff['height']) + " " + ff['ext'],self)
                # self.settingsBox.addWidget(checkBox)
                self.sWidget.addVideocChBox(ff['format_note'] + " " + str(ff['width']) + "x" + str(ff['height']) + " " + ff['ext'],ff['format_id'])
                print(ff.get('width', 'default'))
                print(ff.get('height', 'default'))
            
            
            # print(ff['format_id'])
            # print("!!!!!!!!!!!!!!!!!!!!!!!\n")

    def initUI(self):
        self.youtubeDwnl = myYouTubeDownloader()
        self.dwnlThread = QThread()
        self.settingsBox = QVBoxLayout()
        self.youtubeDwnl.moveToThread(self.dwnlThread)
        self.dwnlThread.start()
        self.sWidget = SettingsWidget()
        inputUrl = QLineEdit(self)
        dwnldBut = QPushButton("Download",self)
        dwnlPgr = QProgressBar(self)
        inputBox = QHBoxLayout()
        inputBox.addWidget(QLabel("Youtube URL",self))
        inputBox.addWidget(inputUrl)
        titleLabel = QLabel("",self)
        descrLabel = QLabel("",self)
        mainVBox = QVBoxLayout()
        mainVBox.addLayout(inputBox)

        titleHBox = QHBoxLayout()
        titleHBox.addWidget(QLabel("Title",self))
        titleHBox.addWidget(titleLabel)

        mainVBox.addLayout(titleHBox)

        descrHBox = QHBoxLayout()
        descrHBox.addWidget(QLabel("Description",self))
        descrHBox.addWidget(descrLabel)

        mainVBox.addLayout(descrHBox)
        mainVBox.addWidget(self.sWidget)

        mainVBox.addWidget(dwnlPgr)
        mainVBox.addWidget(dwnldBut)
        inputUrl.textChanged.connect(self.youtubeDwnl.setUrl)
        dwnldBut.clicked.connect(self.youtubeDwnl.downloadYoutubeUrl)
        self.youtubeDwnl.downloadProgress.connect(dwnlPgr.setValue)
        self.youtubeDwnl.totalBytesChanged.connect(dwnlPgr.setMaximum)
        self.youtubeDwnl.sendTitle.connect(titleLabel.setText)
        self.youtubeDwnl.sendDescription.connect(descrLabel.setText)
        self.youtubeDwnl.sendFormat.connect(self.procFormat)
        self.setLayout(mainVBox)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWidget()
    ex.show()
    print("start")
    sys.exit(app.exec_())