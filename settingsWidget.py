import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import (QWidget,QVBoxLayout,QHBoxLayout,QCheckBox,QLabel,QScrollArea)
from PyQt5 import QtCore 
from myCheckBox import MyCheckBox

class SettingsWidget(QWidget):
    
    mainvbox = QVBoxLayout()

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.scr = QScrollArea(self)
        
        # self.mainvbox.addWidget(self.scr)
        self.mainw = QWidget(self)
        # self.setLayout(self.mainvbox)
        self.mainw.setLayout(self.mainvbox)
        # self.mainw.setMaximumHeight(200)
        self.scr.setWidget(self.mainw)
        # self.scr.setGeometry(QtCore.QRect(5, 5, 390, 190))
        self.scr.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scr.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scr.setWidgetResizable(True)
        self.scr.setObjectName("scrollArea")
        self.scr.setEnabled(True)
        self.mainv = QVBoxLayout()
        self.mainv.addWidget(self.scr)
        self.setLayout(self.mainv)
        # self.scr.setMaximumSize(100, 100)

    def addVideocChBox(self,title,id):
        print('id' + str(id))
        self.mainvbox.addWidget(MyCheckBox(title,id))

if __name__ == '__main__':
    print('start')
    app = QApplication(sys.argv)
    ex = SettingsWidget()
    ex.show()

    # ex.downloadYoutubeUrl()

    sys.exit(app.exec_())